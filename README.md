**Golang 方便好用的配置文件解析器**
---
- 支持多级配置，路径查找
- 支持文件、目录
- 动态监听配置变更

## Install
- go get github.com/plusplus1/goconf 
- go get git.oschina.net/plus1/goconf


## Contact:
    comeson@126.com
    